package utils

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base32"
	"encoding/binary"
	"fmt"
	"net/url"
	"strings"
	"time"
)

func hmacSha1(key, data []byte) []byte {
	h := hmac.New(sha1.New, key)
	if total := len(data); total > 0 {
		h.Write(data)
	}
	return h.Sum(nil)
}

func GetSecret() string {
	var buf bytes.Buffer
	binary.Write(&buf, binary.BigEndian, time.Now().UnixNano())
	return strings.ToUpper(base32.StdEncoding.EncodeToString(hmacSha1(buf.Bytes(), nil)))
}

func ComputeCode(secret string, value int64) (string, error) {
	key, err := base32.StdEncoding.DecodeString(secret)
	if err != nil {
		return "", err
	}
	hash := hmac.New(sha1.New, key)
	err = binary.Write(hash, binary.BigEndian, value)
	if err != nil {
		return "", err
	}
	h := hash.Sum(nil)
	offset := h[19] & 0x0f
	truncated := binary.BigEndian.Uint32(h[offset : offset+4])
	truncated &= 0x7fffffff
	code := truncated % 1000000
	return fmt.Sprintf("%06d", code), nil
}

func GetQrcode(user, secret string) string {
	return fmt.Sprintf("otpauth://totp/%s?secret=%s", user, secret)
}

func GetQrcodeUrl(qrData string) string {
	width := "200"
	height := "200"
	data := url.Values{}
	data.Set("data", qrData)
	return "https://api.qrserver.com/v1/create-qr-code/?" + data.Encode() + "&size=" + width + "x" + height + "&ecc=M"
}

func checkTotpCode(secret, code string, min, max int64) bool {
	for t := min; t <= max; t++ {
		r, err := ComputeCode(secret, t)
		if err == nil && r == code {
			return true
		}
	}
	return false
}

func Authenticate(secret, password string) bool {
	t0 := int64(time.Now().Unix() / 30)
	return checkTotpCode(secret, password, t0-(30/2), t0+(30/2))
}
