package bastion // import "sshportal/pkg/bastion"

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"strconv"
	"strings"
	"sync"
	"time"

	"sshportal/pkg/crypto"
	"sshportal/pkg/dbmodels"
	"sshportal/pkg/utils"
	"sshportal/ssh"

	gossh "golang.org/x/crypto/ssh"
	goagent "golang.org/x/crypto/ssh/agent"
	"gorm.io/gorm"
)

type sshportalContextKey string

var authContextKey = sshportalContextKey("auth")
var forwardedContextKey = sshportalContextKey("forwarded")
var lastClientContextKey = sshportalContextKey("lastClient")
var lastClientLoggingContextKey = sshportalContextKey("lastClientLogging")

type authContext struct {
	message         string
	err             error
	user            dbmodels.User
	inputUsername   string
	db              *gorm.DB
	userKey         dbmodels.UserKey
	logsLocation    string
	aclCheckCmd     string
	aesKey          string
	dbDriver, dbURL string
	bindAddr        string
	demo, debug     bool
	authMethod      string
	authSuccess     bool
}

type forwardedContext struct {
	forwards map[string]net.Listener
	sync.Mutex
}

type userType string

type remoteForwardRequest struct {
	BindAddr string
	BindPort uint32
}

type remoteForwardSuccess struct {
	BindPort uint32
}

type remoteForwardChannelData struct {
	DestAddr   string
	DestPort   uint32
	OriginAddr string
	OriginPort uint32
}

type remoteForwardCancelRequest struct {
	BindAddr string
	BindPort uint32
}

const (
	userTypeHealthcheck userType = "healthcheck"
	userTypeBastion     userType = "bastion"
	userTypeInvite      userType = "invite"
	userTypeShell       userType = "shell"
)

func (c authContext) userType() userType {
	switch {
	case c.inputUsername == "healthcheck":
		return userTypeHealthcheck
	case c.inputUsername == c.user.Name || c.inputUsername == c.user.Email || c.inputUsername == "admin":
		return userTypeShell
	case strings.HasPrefix(c.inputUsername, "invite:"):
		return userTypeInvite
	default:
		return userTypeBastion
	}
}

func dynamicHostKey(db *gorm.DB, host *dbmodels.Host) gossh.HostKeyCallback {
	return func(hostname string, remote net.Addr, key gossh.PublicKey) error {
		if len(host.HostKey) == 0 {
			log.Printf("Discovering host fingerprint %s: %s", host.Name, key.Marshal())
			return db.Model(host).Update("HostKey", key.Marshal()).Error
		}

		if host.SkipKey && !bytes.Equal(host.HostKey, key.Marshal()) {
			log.Printf("Update host fingerprint %s: %s", host.Name, key.Marshal())
			return db.Model(host).Update("HostKey", key.Marshal()).Error
		}

		if !bytes.Equal(host.HostKey, key.Marshal()) {
			return fmt.Errorf("ssh: host key mismatch")
		}
		return nil
	}
}

func chainedSessionConfigs(ctx ssh.Context, actx *authContext, host *dbmodels.Host) ([]sessionConfig, *loggingConfig, error) {
	sessionConfigs := make([]sessionConfig, 0)
	currentHost := host
	var logging *loggingConfig
	for currentHost != nil {
		clientConfig, err := bastionClientConfig(ctx, currentHost)
		if err != nil {
			return nil, nil, err
		}
		sessionConfigs = append([]sessionConfig{{
			Addr:         currentHost.DialAddr(),
			Host:         currentHost.Host(),
			Proxy:        currentHost.Proxy,
			ClientConfig: clientConfig,
			WSPrepadding: currentHost.Scheme() == dbmodels.BastionSchemeWSPSSH,
		}}, sessionConfigs...)
		if logging == nil {
			// first one
			logging = &loggingConfig{
				LoggingMode:  currentHost.Logging,
				LogsLocation: actx.logsLocation,
			}
		}
		if currentHost.HopID != 0 {
			var newHost dbmodels.Host
			if err := actx.db.Model(currentHost).Association("Hop").Find(&newHost); err != nil {
				log.Printf("Error: %v", err)
				return nil, nil, err
			}
			hostname := newHost.Name
			currentHost, _ = dbmodels.HostByName(actx.db, hostname)
		} else {
			currentHost = nil
		}
	}
	return sessionConfigs, logging, nil
}

var DefaultChannelHandler ssh.ChannelHandler = func(srv *ssh.Server, conn *gossh.ServerConn, newChan gossh.NewChannel, ctx ssh.Context) {}

func ChannelHandler(srv *ssh.Server, conn *gossh.ServerConn, newChan gossh.NewChannel, ctx ssh.Context) {
	switch newChan.ChannelType() {
	case "session":
	case "direct-tcpip":
	default:
		// TODO: handle direct-tcp (only for ssh scheme)
		if err := newChan.Reject(gossh.UnknownChannelType, "unsupported channel type"); err != nil {
			log.Printf("error: failed to reject channel: %v", err)
		}
		return
	}

	actx := ctx.Value(authContextKey).(*authContext)

	if actx.user.ID == 0 && actx.userType() != userTypeHealthcheck {
		ip, err := net.ResolveTCPAddr(conn.RemoteAddr().Network(), conn.RemoteAddr().String())
		if err == nil {
			log.Printf("Auth failed: sshUser=%q remote=%q", conn.User(), ip.IP.String())
			actx.err = errors.New("access denied")

			ch, _, err2 := newChan.Accept()
			if err2 != nil {
				return
			}
			fmt.Fprintf(ch, "error: %v\n", actx.err)
			_ = ch.Close()
			return
		}
	}

	switch actx.userType() {
	case userTypeBastion:
		log.Printf("New connection(bastion): sshUser=%q remote=%q local=%q dbUser=id:%d,email:%s", conn.User(), conn.RemoteAddr(), conn.LocalAddr(), actx.user.ID, actx.user.Email)
		host, err := dbmodels.HostByName(actx.db, actx.inputUsername)
		if err != nil {
			ch, _, err2 := newChan.Accept()
			if err2 != nil {
				return
			}
			fmt.Fprintf(ch, "error: %v\n", err)
			// FIXME: force close all channels
			_ = ch.Close()
			return
		}

		switch host.Scheme() {
		case dbmodels.BastionSchemeSSH, dbmodels.BastionSchemeWSPSSH:
			lastClient, ok := ctx.Value(lastClientContextKey).(*gossh.Client)
			if !ok {
				ch, _, err2 := newChan.Accept()
				if err2 != nil {
					return
				}
				fmt.Fprintf(ch, "error: last client is null\n")
				_ = ch.Close()
				return
			}

			logging, ok := ctx.Value(lastClientLoggingContextKey).(*loggingConfig)
			if !ok {
				logging = &loggingConfig{
					LoggingMode: "disabled",
				}
			}
			sess := dbmodels.Session{
				UserID: actx.user.ID,
				HostID: host.ID,
				Status: string(dbmodels.SessionStatusActive),
			}
			if err = actx.db.Create(&sess).Error; err != nil {
				ch, _, err2 := newChan.Accept()
				if err2 != nil {
					return
				}
				fmt.Fprintf(ch, "error: %v\n", err)
				_ = ch.Close()
				return
			}
			go func() {
				err = multiChannelHandler(conn, newChan, lastClient, logging, ctx, sess.ID)
				if err != nil {
					log.Printf("Error: %v", err)
				}

				now := time.Now()
				sessUpdate := dbmodels.Session{
					Status:    string(dbmodels.SessionStatusClosed),
					ErrMsg:    fmt.Sprintf("%v", err),
					StoppedAt: &now,
				}
				if err == nil {
					sessUpdate.ErrMsg = ""
				}
				actx.db.Model(&sess).Updates(&sessUpdate)
			}()
		default:
			ch, _, err2 := newChan.Accept()
			if err2 != nil {
				return
			}
			fmt.Fprintf(ch, "error: unknown bastion scheme: %q\n", host.Scheme())
			// FIXME: force close all channels
			_ = ch.Close()
		}
	default: // shell
		DefaultChannelHandler(srv, conn, newChan, ctx)
	}
}

func RequestHandler(ctx ssh.Context, srv *ssh.Server, req *gossh.Request) (ok bool, payload []byte) {
	actx := ctx.Value(authContextKey).(*authContext)
	// only used for bastion
	if actx.userType() != userTypeBastion {
		return true, []byte{}
	}
	lastClient, ok := ctx.Value(lastClientContextKey).(*gossh.Client)
	if !ok {
		return false, []byte{}
	}
	switch req.Type {
	case "tcpip-forward":
		var reqPayload remoteForwardRequest
		if err := gossh.Unmarshal(req.Payload, &reqPayload); err != nil {
			return false, []byte{}
		}
		var addr string
		if strings.TrimSpace(reqPayload.BindAddr) == "" {
			addr = net.JoinHostPort("0.0.0.0", strconv.Itoa(int(reqPayload.BindPort)))
		} else {
			addr = net.JoinHostPort(reqPayload.BindAddr, strconv.Itoa(int(reqPayload.BindPort)))
		}
		host, err := dbmodels.HostByName(actx.db, actx.inputUsername)
		if err != nil {
			return false, []byte{}
		}

		conn, ok := ctx.Value(ssh.ContextKeyConn).(*gossh.ServerConn)
		if !ok {
			return false, []byte{}
		}
		log.Printf("New request(bastion): sshUser=%q remote=%q local=%q bind:%s dbUser=id:%d,email:%s", conn.User(), conn.RemoteAddr(), conn.LocalAddr(), addr, actx.user.ID, actx.user.Email)
		switch host.Scheme() {
		case dbmodels.BastionSchemeSSH, dbmodels.BastionSchemeWSPSSH:
			ln, err := lastClient.Listen("tcp", addr)
			if err != nil {
				return false, []byte{}
			}
			_, destPortStr, _ := net.SplitHostPort(ln.Addr().String())
			destPort, _ := strconv.Atoi(destPortStr)

			fctx := ctx.Value(forwardedContextKey).(*forwardedContext)
			fctx.Lock()
			fctx.forwards[addr] = ln
			fctx.Unlock()
			go func() {
				<-ctx.Done()
				fctx.Lock()
				ln, ok := fctx.forwards[addr]
				fctx.Unlock()
				if ok {
					ln.Close()
				}
			}()
			go func() {
				for {
					c, err := ln.Accept()
					if err != nil {
						break
					}
					originAddr, orignPortStr, _ := net.SplitHostPort(c.RemoteAddr().String())
					originPort, _ := strconv.Atoi(orignPortStr)
					payload := gossh.Marshal(&remoteForwardChannelData{
						DestAddr:   reqPayload.BindAddr,
						DestPort:   uint32(destPort),
						OriginAddr: originAddr,
						OriginPort: uint32(originPort),
					})
					go func() {
						ch, reqs, err := conn.OpenChannel("forwarded-tcpip", payload)
						if err != nil {
							log.Println(err)
							c.Close()
							return
						}
						go gossh.DiscardRequests(reqs)
						go func() {
							defer ch.Close()
							defer c.Close()
							io.Copy(ch, c)
						}()
						defer ch.Close()
						defer c.Close()
						io.Copy(c, ch)
					}()
				}
				fctx.Lock()
				delete(fctx.forwards, addr)
				fctx.Unlock()
			}()

			return true, gossh.Marshal(&remoteForwardSuccess{uint32(destPort)})
		default:
			return true, []byte{}
		}
	case "cancel-tcpip-forward":
		var reqPayload remoteForwardCancelRequest
		if err := gossh.Unmarshal(req.Payload, &reqPayload); err != nil {
			return false, []byte{}
		}
		addr := net.JoinHostPort(reqPayload.BindAddr, strconv.Itoa(int(reqPayload.BindPort)))
		fctx := ctx.Value(forwardedContextKey).(*forwardedContext)
		fctx.Lock()
		ln, ok := fctx.forwards[addr]
		fctx.Unlock()
		if ok {
			ln.Close()
		}
		return true, nil
	default:
		ok, playload, err := lastClient.SendRequest(req.Type, req.WantReply, req.Payload)
		if err != nil {
			return false, nil
		}
		return ok, playload
	}
}

func NewClientAccept(ctx ssh.Context, proxy string) error {
	actx := ctx.Value(authContextKey).(*authContext)
	// only used for bastion
	if actx.userType() != userTypeBastion {
		return nil
	}
	conn := ctx.Value(ssh.ContextKeyConn).(*gossh.ServerConn)
	host, err := dbmodels.HostByName(actx.db, actx.inputUsername)
	if err != nil {
		return err
	}
	switch host.Scheme() {
	case dbmodels.BastionSchemeSSH, dbmodels.BastionSchemeWSPSSH:
		sessionConfigs, logging, err := chainedSessionConfigs(ctx, actx, host)
		if err != nil {
			return err
		}
		var lastClient *gossh.Client = nil
		sessionCount := len(sessionConfigs)
		for i, config := range sessionConfigs {
			if strings.TrimSpace(config.Proxy) == "" && strings.TrimSpace(proxy) != "" {
				config.Proxy = proxy
			}
			client, err := chainedDial(lastClient, config, config.ClientConfig)
			if err != nil {
				return err
			}
			if i != sessionCount-1 {
				defer func() { _ = client.Close() }()
			}
			lastClient = client
		}
		ctx.SetValue(lastClientContextKey, lastClient)
		ctx.SetValue(lastClientLoggingContextKey, logging)
		channels := lastClient.HandleChannelOpen("auth-agent@openssh.com")
		if channels != nil {
			go func() {
				for ch := range channels {
					channel, reqs, err := ch.Accept()
					if err != nil {
						continue
					}
					agentChan, agentReqs, err := conn.OpenChannel("auth-agent@openssh.com", nil)
					if err != nil {
						continue
					}
					go gossh.DiscardRequests(reqs)
					go gossh.DiscardRequests(agentReqs)
					go func() {
						defer agentChan.Close()
						defer channel.Close()
						ag := goagent.NewClient(agentChan)
						goagent.ServeAgent(ag, channel)
					}()
				}
			}()
		}
		return nil
	default:
		return fmt.Errorf("error: unknown bastion scheme: %q", host.Scheme())
	}
}

func ClientClosed(ctx ssh.Context) error {
	if lastClient, ok := ctx.Value(lastClientContextKey).(*gossh.Client); ok {
		return lastClient.Close()
	} else {
		return fmt.Errorf("last client is nil")
	}
}

func bastionClientConfig(ctx ssh.Context, host *dbmodels.Host) (*gossh.ClientConfig, error) {
	actx := ctx.Value(authContextKey).(*authContext)

	crypto.HostDecrypt(actx.aesKey, host)
	crypto.SSHKeyDecrypt(actx.aesKey, host.SSHKey)

	clientConfig, err := host.ClientConfig(dynamicHostKey(actx.db, host))
	if err != nil {
		return nil, err
	}

	var tmpUser dbmodels.User
	if err = actx.db.Preload("Groups").Preload("Groups.ACLs").Where("id = ?", actx.user.ID).First(&tmpUser).Error; err != nil {
		return nil, err
	}
	var tmpHost dbmodels.Host
	if err = actx.db.Preload("Groups").Preload("Groups.ACLs").Where("id = ?", host.ID).First(&tmpHost).Error; err != nil {
		return nil, err
	}

	action := checkACLs(tmpUser, tmpHost, actx.aclCheckCmd)
	switch action {
	case string(dbmodels.ACLActionAllow):
		// do nothing
	case string(dbmodels.ACLActionDeny):
		return nil, fmt.Errorf("you don't have permission to that host")
	default:
		return nil, fmt.Errorf("invalid ACL action: %q", action)
	}
	return clientConfig, nil
}

func ShellHandler(s ssh.Session, version, gitSha, gitTag string) {
	actx := s.Context().Value(authContextKey).(*authContext)
	if actx.userType() != userTypeHealthcheck {
		log.Printf("New connection(shell): sshUser=%q remote=%q local=%q command=%q dbUser=id:%d,email:%s", s.User(), s.RemoteAddr(), s.LocalAddr(), s.Command(), actx.user.ID, actx.user.Email)
	}

	if actx.err != nil {
		fmt.Fprintf(s, "error: %v\n", actx.err)
		_ = s.Exit(1)
		return
	}

	if actx.message != "" {
		fmt.Fprint(s, actx.message)
	}

	switch actx.userType() {
	case userTypeHealthcheck:
		fmt.Fprintln(s, "OK")
		return
	case userTypeShell:
		if err := shell(s, version, gitSha, gitTag); err != nil {
			fmt.Fprintf(s, "error: %v\n", err)
			_ = s.Exit(1)
		}
		return
	case userTypeInvite:
		// do nothing (message was printed at the beginning of the function)
		return
	}
	panic("should not happen")
}

func PasswordAuthHandler(db *gorm.DB, logsLocation, aclCheckCmd, aesKey, dbDriver, dbURL, bindAddr string, demo bool) ssh.PasswordHandler {
	return func(ctx ssh.Context, pass string) bool {
		actx := &authContext{
			db:            db,
			inputUsername: ctx.User(),
			logsLocation:  logsLocation,
			aclCheckCmd:   aclCheckCmd,
			aesKey:        aesKey,
			dbDriver:      dbDriver,
			dbURL:         dbURL,
			bindAddr:      bindAddr,
			demo:          demo,
			authMethod:    "password",
			authSuccess:   true,
		}
		ctx.SetValue(authContextKey, actx)
		if actx.userType() == userTypeHealthcheck {
			return true
		} else {
			splitedUsers := strings.Split(actx.inputUsername, "@")
			if len(splitedUsers) < 2 {
				return false
			}
			db.Preload("Groups").Preload("Keys").Preload("Roles").Where("name = ?", splitedUsers[0]).First(&actx.user)
			db.Where("user_id = ?", actx.user.ID).First(&actx.userKey)

			if actx.user.ID > 0 {
				r := utils.Authenticate(actx.userKey.Otp, pass)
				if !r {
					return false
				}
				fctx := &forwardedContext{
					forwards: make(map[string]net.Listener),
				}
				actx.inputUsername = strings.Replace(actx.inputUsername, actx.user.Name+"@", "", 1)
				ctx.SetValue(forwardedContextKey, fctx)
				return true
			} else {
				return false
			}
		}
	}
}

func PrivateKeyFromDB(db *gorm.DB, aesKey string) func(*ssh.Server) error {
	return func(srv *ssh.Server) error {
		var key dbmodels.SSHKey
		if err := dbmodels.SSHKeysByIdentifiers(db, []string{"host"}).First(&key).Error; err != nil {
			return err
		}
		crypto.SSHKeyDecrypt(aesKey, &key)

		signer, err := gossh.ParsePrivateKey([]byte(key.PrivKey))
		if err != nil {
			return err
		}
		srv.AddHostKey(signer)
		return nil
	}
}

func PublicKeyAuthHandler(db *gorm.DB, logsLocation, aclCheckCmd, aesKey, dbDriver, dbURL, bindAddr string, demo bool) ssh.PublicKeyHandler {
	return func(ctx ssh.Context, key ssh.PublicKey) bool {
		actx := &authContext{
			db:            db,
			inputUsername: ctx.User(),
			logsLocation:  logsLocation,
			aclCheckCmd:   aclCheckCmd,
			aesKey:        aesKey,
			dbDriver:      dbDriver,
			dbURL:         dbURL,
			bindAddr:      bindAddr,
			demo:          demo,
			authMethod:    "pubkey",
			authSuccess:   true,
		}
		ctx.SetValue(authContextKey, actx)

		fctx := &forwardedContext{
			forwards: make(map[string]net.Listener),
		}
		ctx.SetValue(forwardedContextKey, fctx)

		// lookup user by key
		db.Where("authorized_key = ?", string(gossh.MarshalAuthorizedKey(key))).First(&actx.userKey)
		if actx.userKey.UserID > 0 {
			db.Preload("Roles").Where("id = ?", actx.userKey.UserID).First(&actx.user)
			if actx.userType() == userTypeInvite {
				actx.err = fmt.Errorf("invites are only supported for new SSH keys; your ssh key is already associated with the user %q", actx.user.Email)
			}
			if strings.Contains(actx.inputUsername, actx.user.Name) {
				_, err := dbmodels.HostByName(db, actx.inputUsername)
				if err != nil {
					actx.inputUsername = strings.Replace(actx.inputUsername, actx.user.Name+"@", "", 1)
				}
			}
			return true
		}

		// handle invite "links"
		if actx.userType() == userTypeInvite {
			inputToken := strings.Split(actx.inputUsername, ":")[1]
			if len(inputToken) > 0 {
				db.Where("invite_token = ?", inputToken).First(&actx.user)
			}
			if actx.user.ID > 0 {
				secret := utils.GetSecret()
				actx.userKey = dbmodels.UserKey{
					UserID:        actx.user.ID,
					Key:           key.Marshal(),
					Comment:       "created by sshportal",
					AuthorizedKey: string(gossh.MarshalAuthorizedKey(key)),
					Otp:           secret,
				}
				db.Create(&actx.userKey)

				// token is only usable once
				actx.user.InviteToken = ""
				db.Model(&actx.user).Updates(&actx.user)

				qrData := utils.GetQrcode(fmt.Sprintf("%s: %s", actx.user.Name, actx.user.Email), secret)
				qrUrl := utils.GetQrcodeUrl(qrData)
				actx.message = fmt.Sprintf("Welcome %s!\n\nYour key is now associated with the user %q.\n\nyour Google Authenticator is: %s\nyou can open this url to bind: %s\n", actx.user.Name, actx.user.Email, qrData, qrUrl)
			} else {
				actx.user = dbmodels.User{Name: "Anonymous"}
				actx.err = errors.New("your token is invalid or expired")
			}
			return true
		}

		if actx.user.ID == 0 {
			return false
		}
		// fallback
		actx.err = errors.New("unknown ssh key")
		actx.user = dbmodels.User{Name: "Anonymous"}
		return true
	}
}
