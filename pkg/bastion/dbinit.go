package bastion // import "sshportal/pkg/bastion"

import (
	"crypto/rand"
	"fmt"
	"io"
	"log"
	"math/big"
	"os"
	"os/user"
	"strings"

	"sshportal/pkg/crypto"
	"sshportal/pkg/dbmodels"

	gormigrate "github.com/go-gormigrate/gormigrate/v2"
	gossh "golang.org/x/crypto/ssh"
	"gorm.io/gorm"
)

func DBInit(db *gorm.DB) error {
	log.SetOutput(io.Discard)
	log.SetOutput(os.Stderr)

	m := gormigrate.New(db, gormigrate.DefaultOptions, []*gormigrate.Migration{
		{
			ID: "1",
			Migrate: func(tx *gorm.DB) error {
				return tx.AutoMigrate(&dbmodels.Setting{})
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.Migrator().DropTable("settings")
			},
		}, {
			ID: "2",
			Migrate: func(tx *gorm.DB) error {
				return tx.AutoMigrate(&dbmodels.SSHKey{})
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.Migrator().DropTable("ssh_keys")
			},
		}, {
			ID: "3",
			Migrate: func(tx *gorm.DB) error {
				return tx.AutoMigrate(&dbmodels.HostGroup{})
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.Migrator().DropTable("hosts_group")
			},
		}, {
			ID: "4",
			Migrate: func(tx *gorm.DB) error {
				return tx.AutoMigrate(&dbmodels.Host{})
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.Migrator().DropTable("host")
			},
		}, {
			ID: "5",
			Migrate: func(tx *gorm.DB) error {
				return tx.AutoMigrate(&dbmodels.UserRole{})
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.Migrator().DropTable("user_role")
			},
		}, {
			ID: "6",
			Migrate: func(tx *gorm.DB) error {
				return tx.AutoMigrate(&dbmodels.UserGroup{})
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.Migrator().DropTable("user_groups")
			},
		}, {
			ID: "7",
			Migrate: func(tx *gorm.DB) error {
				return tx.AutoMigrate(&dbmodels.User{})
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.Migrator().DropTable("user")
			},
		}, {
			ID: "8",
			Migrate: func(tx *gorm.DB) error {
				return tx.AutoMigrate(&dbmodels.UserKey{})
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.Migrator().DropTable("user_key")
			},
		}, {
			ID: "9",
			Migrate: func(tx *gorm.DB) error {
				return tx.AutoMigrate(&dbmodels.ACL{})
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.Migrator().DropTable("acl")
			},
		}, {
			ID: "10",
			Migrate: func(tx *gorm.DB) error {
				return tx.AutoMigrate(&dbmodels.Session{})
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.Migrator().DropTable("session")
			},
		}, {
			ID: "11",
			Migrate: func(tx *gorm.DB) error {
				return tx.AutoMigrate(&dbmodels.Event{})
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.Migrator().DropTable("event")
			},
		}, {
			ID: "13",
			Migrate: func(tx *gorm.DB) error {
				return tx.Create(&dbmodels.UserRole{Name: "admin"}).Error
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.Where("name = ?", "admin").Unscoped().Delete(&dbmodels.UserRole{}).Error
			},
		}, {
			ID: "14",
			Migrate: func(tx *gorm.DB) error {
				return tx.Create(&dbmodels.UserRole{Name: "listhosts"}).Error
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.Where("name = ?", "listhosts").Unscoped().Delete(&dbmodels.UserRole{}).Error
			},
		}, {
			ID: "15",
			Migrate: func(tx *gorm.DB) error {
				var userKeys []*dbmodels.UserKey
				if err := db.Find(&userKeys).Error; err != nil {
					return err
				}

				for _, userKey := range userKeys {
					key, err := gossh.ParsePublicKey(userKey.Key)
					if err != nil {
						return err
					}
					userKey.AuthorizedKey = string(gossh.MarshalAuthorizedKey(key))
					if err := db.Model(userKey).Updates(userKey).Error; err != nil {
						return err
					}
				}
				return nil
			},
			Rollback: func(tx *gorm.DB) error {
				return fmt.Errorf("not implemented")
			},
		}, {
			ID: "16",
			Migrate: func(tx *gorm.DB) error {
				var sessions []*dbmodels.Session
				if err := db.Find(&sessions).Error; err != nil {
					return err
				}

				for _, session := range sessions {
					if session.StoppedAt != nil && session.StoppedAt.IsZero() {
						if err := db.Model(session).Updates(map[string]interface{}{"stopped_at": nil}).Error; err != nil {
							return err
						}
					}
				}
				return nil
			},
			Rollback: func(tx *gorm.DB) error {
				return fmt.Errorf("not implemented")
			},
		}, {
			ID: "17",
			Migrate: func(tx *gorm.DB) error {
				return tx.Session(&gorm.Session{AllowGlobalUpdate: true}).Model(&dbmodels.Host{}).Updates(&dbmodels.Host{Logging: "disabled"}).Error
			},
			Rollback: func(tx *gorm.DB) error { return fmt.Errorf("not implemented") },
		},
	})
	if err := m.Migrate(); err != nil {
		return err
	}
	dbmodels.NewEvent("system", "migrated").Log(db)

	// create default ssh key
	var count int64
	if err := db.Table("ssh_keys").Where("name = ?", "default").Count(&count).Error; err != nil {
		return err
	}
	if count == 0 {
		key, err := crypto.NewSSHKey("ed25519", 1)
		if err != nil {
			return err
		}
		key.Name = "default"
		key.Comment = "created by sshportal"
		if err := db.Create(&key).Error; err != nil {
			return err
		}
	}

	// create default host group
	if err := db.Table("host_groups").Where("name = ?", "default").Count(&count).Error; err != nil {
		return err
	}
	if count == 0 {
		hostGroup := dbmodels.HostGroup{
			Name:    "default",
			Comment: "created by sshportal",
		}
		if err := db.Create(&hostGroup).Error; err != nil {
			return err
		}
	}

	// create default user group
	if err := db.Table("user_groups").Where("name = ?", "default").Count(&count).Error; err != nil {
		return err
	}
	if count == 0 {
		userGroup := dbmodels.UserGroup{
			Name:    "default",
			Comment: "created by sshportal",
		}
		if err := db.Create(&userGroup).Error; err != nil {
			return err
		}
	}

	// create default acl
	if err := db.Table("acls").Count(&count).Error; err != nil {
		return err
	}
	if count == 0 {
		var defaultUserGroup dbmodels.UserGroup
		db.Where("name = ?", "default").First(&defaultUserGroup)
		var defaultHostGroup dbmodels.HostGroup
		db.Where("name = ?", "default").First(&defaultHostGroup)
		acl := dbmodels.ACL{
			UserGroups: []*dbmodels.UserGroup{&defaultUserGroup},
			HostGroups: []*dbmodels.HostGroup{&defaultHostGroup},
			Action:     "allow",
			//HostPattern: "",
			//Weight:      0,
			Comment: "created by sshportal",
		}
		if err := db.Create(&acl).Error; err != nil {
			return err
		}
	}

	// create admin user
	var defaultUserGroup dbmodels.UserGroup
	db.Where("name = ?", "default").First(&defaultUserGroup)
	if err := db.Table("users").Count(&count).Error; err != nil {
		return err
	}
	if count == 0 {
		// if no admin, create an account for the first connection
		inviteToken, err := randStringBytes(16)
		if err != nil {
			return err
		}
		if os.Getenv("SSHPORTAL_DEFAULT_ADMIN_INVITE_TOKEN") != "" {
			inviteToken = os.Getenv("SSHPORTAL_DEFAULT_ADMIN_INVITE_TOKEN")
		}
		var adminRole dbmodels.UserRole
		if err := db.Where("name = ?", "admin").First(&adminRole).Error; err != nil {
			return err
		}
		var username string
		if currentUser, err := user.Current(); err == nil {
			username = currentUser.Username
		}
		if username == "" {
			username = os.Getenv("USER")
		}
		username = strings.ToLower(username)
		if username == "" {
			username = "admin" // fallback username
		}
		user := dbmodels.User{
			Name:        username,
			Email:       fmt.Sprintf("%s@localhost", username),
			Comment:     "created by sshportal",
			Roles:       []*dbmodels.UserRole{&adminRole},
			InviteToken: inviteToken,
			Groups:      []*dbmodels.UserGroup{&defaultUserGroup},
		}
		if err := db.Create(&user).Error; err != nil {
			return err
		}
		log.Printf("info 'admin' user created, use the user 'invite:%s' to associate a public key with this account", user.InviteToken)
	}

	// create host ssh key
	if err := db.Table("ssh_keys").Where("name = ?", "host").Count(&count).Error; err != nil {
		return err
	}
	if count == 0 {
		key, err := crypto.NewSSHKey("ed25519", 1)
		if err != nil {
			return err
		}
		key.Name = "host"
		key.Comment = "created by sshportal"
		if err := db.Create(&key).Error; err != nil {
			return err
		}
	}

	// close unclosed connections
	return db.Table("sessions").Where("status = ?", "active").Updates(&dbmodels.Session{
		Status: string(dbmodels.SessionStatusClosed),
		ErrMsg: "sshportal was halted while the connection was still active",
	}).Error
}

func randStringBytes(n int) (string, error) {
	const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	b := make([]byte, n)
	for i := range b {
		r, err := rand.Int(rand.Reader, big.NewInt(int64(len(letterBytes))))
		if err != nil {
			return "", fmt.Errorf("failed to generate random string: %s", err)
		}
		b[i] = letterBytes[r.Int64()]
	}
	return string(b), nil
}
