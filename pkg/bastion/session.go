package bastion // import "sshportal/pkg/bastion"

import (
	"fmt"
	"io"
	"log"
	"net"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"

	"sshportal/pkg/utils"
	"sshportal/ssh"

	gossh "golang.org/x/crypto/ssh"
	goproxy "golang.org/x/net/proxy"
)

type direct struct{}

var Direct = direct{}

func (direct) Dial(network, addr string) (net.Conn, error) {
	return net.Dial(network, addr)
}

type sessionConfig struct {
	Addr         string
	Host         string
	Proxy        string
	ClientConfig *gossh.ClientConfig
	WSPrepadding bool
}

type loggingConfig struct {
	LogsLocation string
	LoggingMode  string
}

func prepaddingHandsShake(conn net.Conn, host string) error {
	x := fmt.Sprintf("GET / HTTP/1.1\r\nHost: %s\r\nConnection: Upgrade\r\nUpgrade: websocket\r\n\r\n", host)
	n, err := conn.Write([]byte(x))
	if err != nil || n != len(x) {
		return fmt.Errorf("write ws header fail: %s", err.Error())
	}

	header := make([]byte, 0)
	one_byte := make([]byte, 1)
	for {
		n, err = conn.Read(one_byte)
		if err != nil || n == 0 {
			return fmt.Errorf("read ws header fail: %s", err.Error())
		}
		header = append(header, one_byte[0])

		if one_byte[0] == '\n' {
			l := len(header)
			if l > 4 {
				if header[l-2] == '\r' && header[l-3] == '\n' && header[l-4] == '\r' {
					if !strings.HasPrefix(string(header), "HTTP/1.1 101") {
						return fmt.Errorf("%s", header)
					}
					break
				}
			}
		}
	}
	return nil
}

func chainedDial(client *gossh.Client, session sessionConfig, config *gossh.ClientConfig) (*gossh.Client, error) {
	var err error
	var rconn net.Conn
	if client == nil {
		if strings.TrimSpace(session.Proxy) != "" {
			proxyURI, err := url.Parse(session.Proxy)
			if err != nil {
				return nil, err
			}
			proxyDialer, err := goproxy.FromURL(proxyURI, Direct)
			if err != nil {
				return nil, err
			}
			rconn, err = proxyDialer.Dial("tcp", session.Addr)
			if err != nil {
				return nil, err
			}
		} else {
			rconn, err = net.Dial("tcp", session.Addr)
			if err != nil {
				return nil, err
			}
		}
	} else {
		rconn, err = client.Dial("tcp", session.Addr)
		if err != nil {
			return nil, err
		}
	}
	if session.WSPrepadding {
		if err := prepaddingHandsShake(rconn, session.Host); err != nil {
			rconn.Close()
			return nil, err
		}
	}
	ncc, chans, reqs, err := gossh.NewClientConn(rconn, session.Addr, config)
	if err != nil {
		rconn.Close()
		return nil, err
	}
	currentClient := gossh.NewClient(ncc, chans, reqs)
	return currentClient, nil
}

func multiChannelHandler(conn *gossh.ServerConn, newChan gossh.NewChannel, lastClient *gossh.Client, logging *loggingConfig, ctx ssh.Context, sessionID uint) error {
	switch newChan.ChannelType() {
	case "session":
		lch, lreqs, err := newChan.Accept()
		// TODO: defer clean closer
		if err != nil {
			// TODO: trigger event callback
			return nil
		}

		rch, rreqs, err := lastClient.OpenChannel("session", []byte{})
		if err != nil {
			return err
		}

		user := conn.User()
		actx := ctx.Value(authContextKey).(*authContext)
		username := actx.user.Name
		// pipe everything
		return pipe(lreqs, rreqs, lch, rch, logging, user, username, sessionID, newChan)
	case "direct-tcpip":
		lch, lreqs, err := newChan.Accept()
		// TODO: defer clean closer
		if err != nil {
			// TODO: trigger event callback
			return nil
		}

		rch, rreqs, err := lastClient.OpenChannel("direct-tcpip", newChan.ExtraData())
		if err != nil {
			return err
		}
		user := conn.User()
		actx := ctx.Value(authContextKey).(*authContext)
		username := actx.user.Name
		// pipe everything
		return pipe(lreqs, rreqs, lch, rch, logging, user, username, sessionID, newChan)
	default:
		if err := newChan.Reject(gossh.UnknownChannelType, "unsupported channel type"); err != nil {
			log.Printf("failed to reject chan: %v", err)
		}
		return nil
	}
}

func pipe(lreqs, rreqs <-chan *gossh.Request, lch, rch gossh.Channel, logging *loggingConfig, user string, username string, sessionID uint, newChan gossh.NewChannel) error {
	defer func() {
		_ = lch.Close()
		_ = rch.Close()
	}()

	errch := make(chan error, 1)
	quit := make(chan string, 1)
	channeltype := newChan.ChannelType()

	var logWriter io.WriteCloser = newDiscardWriteCloser()
	if logging.LoggingMode != "disabled" {
		filename := filepath.Join(logging.LogsLocation, fmt.Sprintf("%s-%s-%s-%d-%s", user, username, channeltype, sessionID, time.Now().Format(time.RFC3339)))
		f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0440)
		if err != nil {
			return fmt.Errorf("open log file: %+v", err)
		}
		defer func() {
			_ = f.Close()
		}()
		log.Printf("Session %v is recorded in %v", channeltype, filename)
		logWriter = f
	}

	if channeltype == "session" {
		switch logging.LoggingMode {
		case "input":
			wrappedrch := utils.NewLogChannel(rch, logWriter)
			go func(quit chan string) {
				_, _ = io.Copy(lch, rch)
				quit <- "rch"
			}(quit)
			go func(quit chan string) {
				_, _ = io.Copy(wrappedrch, lch)
				quit <- "lch"
			}(quit)
		default: // everything, disabled
			wrappedlch := utils.NewLogChannel(lch, logWriter)
			go func(quit chan string) {
				_, _ = io.Copy(wrappedlch, rch)
				quit <- "rch"
			}(quit)
			go func(quit chan string) {
				_, _ = io.Copy(rch, lch)
				quit <- "lch"
			}(quit)
		}
	}
	if channeltype == "direct-tcpip" {
		d := logTunnelForwardData{}
		if err := gossh.Unmarshal(newChan.ExtraData(), &d); err != nil {
			return err
		}
		wrappedlch := newLogTunnel(lch, logWriter, d.SourceHost)
		wrappedrch := newLogTunnel(rch, logWriter, d.DestinationHost)
		go func(quit chan string) {
			_, _ = io.Copy(wrappedlch, rch)
			quit <- "rch"
		}(quit)

		go func(quit chan string) {
			_, _ = io.Copy(wrappedrch, lch)
			quit <- "lch"
		}(quit)
	}

	go func(quit chan string) {
		for req := range lreqs {
			b, err := rch.SendRequest(req.Type, req.WantReply, req.Payload)
			if req.Type == "exec" {
				wrappedlch := utils.NewLogChannel(lch, logWriter)
				req.Payload = append(req.Payload, []byte("\n")...)
				if _, err := wrappedlch.LogWrite(req.Payload); err != nil {
					log.Printf("failed to write log: %v", err)
				}
			}

			if err != nil {
				errch <- err
			}
			if err2 := req.Reply(b, nil); err2 != nil {
				errch <- err2
			}
		}
		quit <- "lreqs"
	}(quit)

	go func(quit chan string) {
		for req := range rreqs {
			b, err := lch.SendRequest(req.Type, req.WantReply, req.Payload)
			if err != nil {
				errch <- err
			}
			if err2 := req.Reply(b, nil); err2 != nil {
				errch <- err2
			}
		}
		quit <- "rreqs"
	}(quit)

	lchEOF, rchEOF, lchClosed, rchClosed := false, false, false, false
	for {
		select {
		case err := <-errch:
			return err
		case q := <-quit:
			switch q {
			case "lch":
				lchEOF = true
				_ = rch.CloseWrite()
			case "rch":
				rchEOF = true
				_ = lch.CloseWrite()
			case "lreqs":
				lchClosed = true
			case "rreqs":
				rchClosed = true
			}

			if lchEOF && lchClosed && !rchClosed {
				rch.Close()
			}

			if rchEOF && rchClosed && !lchClosed {
				lch.Close()
			}

			if lchEOF && rchEOF && lchClosed && rchClosed {
				return nil
			}
		}
	}
}

func newDiscardWriteCloser() io.WriteCloser { return &discardWriteCloser{io.Discard} }

type discardWriteCloser struct {
	io.Writer
}

func (discardWriteCloser) Close() error {
	return nil
}
