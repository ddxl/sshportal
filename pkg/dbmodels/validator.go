package dbmodels

import (
	"net/url"
	"regexp"

	"github.com/asaskevich/govalidator"
)

func InitValidator() {
	unixUserRegexp := regexp.MustCompile("[a-z_][a-z0-9_-]*")

	govalidator.CustomTypeTagMap.Set("unix_user", govalidator.CustomTypeValidator(func(i interface{}, context interface{}) bool {
		name, ok := i.(string)
		if !ok {
			return false
		}
		return unixUserRegexp.MatchString(name)
	}))
	govalidator.CustomTypeTagMap.Set("host_logging_mode", govalidator.CustomTypeValidator(func(i interface{}, context interface{}) bool {
		name, ok := i.(string)
		if !ok {
			return false
		}
		if name == "" {
			return true
		}
		return IsValidHostLoggingMode(name)
	}))
	govalidator.CustomTypeTagMap.Set("host_proxy_addr", govalidator.CustomTypeValidator(func(i interface{}, context interface{}) bool {
		name, ok := i.(string)
		if !ok {
			return false
		}
		if name == "" {
			return true
		}
		return IsValidProxyAddr(name)
	}))
}

func IsValidHostLoggingMode(name string) bool {
	return name == "disabled" || name == "input" || name == "everything"
}

func IsValidProxyAddr(name string) bool {
	u, err := url.Parse(name)
	if err != nil {
		return false
	}
	return u.Scheme == "socks5" || u.Scheme == "http" || u.Scheme == "socks5h"
}
