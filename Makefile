VERSION ?= `git describe --tags --always`
VCS_REF ?= `git rev-parse --short HEAD`
GO_BUILD_OPTS = -ldflags="-extldflags '-static' -X main.GitSha=$(VCS_REF) -X main.GitTag=$(VERSION)"

all: dist/sshportal

.PHONY: dist/sshportal
dist/sshportal:
	CGO_ENABLED=1 go build $(GO_BUILD_OPTS) -tags netgo -o dist/sshportal